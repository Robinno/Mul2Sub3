/**
 * represents a position in the tree
 * 
 * @author robin nollet
 *
 * @param <E>
 *            type of the element at the position in the tree
 */

public interface TreePosition<E> {
	/**
	 * 
	 * @return element at position in the tree
	 */
	public E getElement();
}
