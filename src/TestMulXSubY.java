import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestMulXSubY {

	@BeforeEach
	void setUp() {
		System.out.println();
	}

	@Test
	void test() throws InvalidTreePositionException, InvalidSideException, TargetNotFoundWithinRangeException {
		MulXSubY mul2sub3 = new MulXSubY(8, 11, 2, 3);
		String uitkomst = mul2sub3.padToString();
		System.out.println(uitkomst);
		System.out.println("aantal berekeningen nodig gehad: " + mul2sub3.getAantalBerekeningen());
		assertEquals(uitkomst, "8 - 3 => 5 * 2 => 10 - 3 => 7 * 2 => 14 - 3 => 11");
	}

	
	@Test
	void test2() throws InvalidTreePositionException, InvalidSideException, TargetNotFoundWithinRangeException {
		MulXSubY mul5sub2 = new MulXSubY(5, 55, 5, 2);
		String uitkomst = mul5sub2.padToString();
		System.out.println(uitkomst);
		System.out.println("aantal berekeningen nodig gehad: " + mul5sub2.getAantalBerekeningen());
		assertEquals(uitkomst, "5 - 2 => 3 * 5 => 15 - 2 => 13 - 2 => 11 * 5 => 55");
	}

}
