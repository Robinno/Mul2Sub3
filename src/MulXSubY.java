public class MulXSubY {

	// attributen

	private BinaryTree<Integer> boom;
	private int x, y, doel;
	private Queue<TreePosition<Integer>> queue = new Queue<TreePosition<Integer>>();
	private LinkedList<Integer> alleElementen = new LinkedList<Integer>();

	// constructor
	public MulXSubY(int start, int doel, int x, int y) {
		boom = new BinaryTree<Integer>(start);
		this.doel = doel;
		this.x = x;
		this.y = y;
	}

	/**
	 * zet het pad van een stack van treepositions naar een string van start naar
	 * doel
	 * 
	 * @throws InvalidTreePositionException
	 * @throws InvalidSideException
	 * @throws TargetNotFoundWithinRangeException
	 */
	public String padToString()
			throws InvalidTreePositionException, InvalidSideException, TargetNotFoundWithinRangeException {
		Stack<TreePosition<Integer>> pad = boom.getPath(bouwBoomTotDoel(boom.getRoot()));
		String padString = "";

		// hieronder zit nog een foutje in

		padString += pad.top().getElement();

		int onderbovenste = pad.top().getElement();
		int bovenste;

		while (onderbovenste != doel) {
			bovenste = pad.pop().getElement();
			onderbovenste = pad.top().getElement();

			if (onderbovenste == bovenste * x) {
				padString += " * " + x + " => " + onderbovenste;
			} else if (onderbovenste == bovenste - y) {
				padString += " - " + y + " => " + onderbovenste;
			} else {
				System.out.println("er is iets verkeerd gegaan");
			}
		}

		return padString;
	}

	/**
	 * dit is het belangrijkste algoritme: het bouwt de tree in de breedte op totdat
	 * het doelelement gevonden is
	 * 
	 * @param root
	 *            (het is recursief)
	 * @return Treeposition van het doel
	 * @throws InvalidSideException
	 * @throws InvalidTreePositionException
	 * @throws TargetNotFoundWithinRangeException
	 */
	private TreePosition<Integer> bouwBoomTotDoel(TreePosition<Integer> cursor)
			throws InvalidSideException, InvalidTreePositionException, TargetNotFoundWithinRangeException {

		// hier komt het algoritme

		if (boom.getLevel(cursor) >= 100) {
			throw new TargetNotFoundWithinRangeException();
		}

		if (cursor.getElement() == doel) {
			return cursor;
		}

		int linkerelement = (int) cursor.getElement() * x;
		int rechterelement = (int) cursor.getElement() - y;

		/*
		 * Het algoritme geoptimaliseerd door te kijken of het element al is
		 * voorgekomen. We weten dat het niet de korste route zal zijn naar het element
		 * (want het is al voorgekomen) en alle berekeningen erna zijn al eens gedaan.
		 */

		if (!alleElementen.contains(linkerelement)) {
			boom.addChild(cursor, linkerelement, "left");
			queue.enQueue(boom.getLeftChild(cursor));
			alleElementen.append(linkerelement);
		}

		if (!alleElementen.contains(rechterelement)) {
			boom.addChild(cursor, rechterelement, "right");
			queue.enQueue(boom.getRightChild(cursor));
			alleElementen.append(linkerelement);
		}

		return bouwBoomTotDoel(queue.dequeue());
	}

	/**
	 * @return het aantal berekeningen dat nodig waren om het pad te vinden
	 */
	public int getAantalBerekeningen() {
		return boom.size() - 1;// min 1 want de root wordt niet berekend, daarna 1 berekening per kind
	}
}
