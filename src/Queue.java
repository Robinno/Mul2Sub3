
public class Queue<E> {
	// attributen
	private LinkedList<E> list = new LinkedList<E>();

	// methoden

	/**
	 * @return de lengte van de queue
	 */
	public int size() {
		return list.getSize();
	}

	/**
	 * @return true als de queue leeg is
	 */
	public boolean isEmpty() {
		return list.isEmpty();
	}

	/**
	 * toevoegen van een element aan de queue
	 * 
	 * @param element
	 */
	public void enQueue(E element) {
		list.append(element);// foutje komt uit de linked list
	}

	/**
	 * hiermee kan je het eerste element uit de queue bekijken, zonder dat die
	 * verwijderd wordt uit de queue
	 */
	public E viewFirst() {
		return list.getHead();
	}

	/**
	 * @return het eerste element uit de queue
	 */
	public E dequeue() {
		E temp = list.getHead();
		list.remove(temp);
		return temp;

	}
}
