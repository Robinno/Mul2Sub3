import java.util.EmptyStackException;

public class StackAdapter<E> implements Stack<E> {

	// attributen

	private LinkedList<E> list;

	// constructor
	public StackAdapter() {
		list = new LinkedList<E>();
	}

	/**
	 * pushes an element on the stack
	 */
	@Override
	public void push(E element) {
		list.prepend(element);
	}

	/**
	 * returns the element on top of the stack, without removing the element from
	 * the stack
	 */
	@Override
	public E top() {
		if (isEmpty())
			throw new EmptyStackException();
		return list.getHead();
	}

	/**
	 * returns the element on top of the stack, and also it gets removed from the
	 * stack
	 */
	@Override
	public E pop() {
		E element = top();
		list = list.getTail();
		return element;
	}

	/**
	 * returns the size of the stack
	 */
	@Override
	public int size() {
		return list.getSize();
	}

	/**
	 * returns true if the stack is empty
	 */
	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}

	/**
	 * removes the lowest node from the stack
	 */
	public void removeLowestNode() {

		LinkedList<E> cursor = list;
		do {
			cursor = cursor.getTail();
		} while (cursor.getSize() > 1);

		list.remove(cursor.getHead());
	}

}
