public class BinaryTree<E> {

	// attributen

	private TreeNode root;
	private int size; // deze size is NIET de hoogte!

	// constructor
	public BinaryTree() {
		root = null;
		size = 0;
	}

	// constructor
	public BinaryTree(E element) {
		root = new TreeNode(element);
		size = 1;
	}

	/**
	 * @return aantal elementen in de boom
	 */
	public int size() {
		return size;
	}

	/**
	 * @return true als de boom leeg is
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * @return de root van de boom
	 */
	public TreePosition<E> getRoot() {
		return root; // hier gebeurt impliciet een cast naar een TreePosition
	}

	/**
	 * @param treepositie
	 * @return het pad (in treeposities) van de root naar de gegeven treepositie via
	 *         een stack (bovenste element = de root)
	 * @throws InvalidTreePositionException
	 */
	public Stack<TreePosition<E>> getPath(TreePosition<E> cursor) throws InvalidTreePositionException {
		TreeNode pos = validate(cursor);
		Stack<TreePosition<E>> stack = new StackAdapter<TreePosition<E>>();

		do {
			stack.push((TreePosition<E>) pos);
			pos = pos.getParent();
		} while (!pos.isRoot());

		stack.push(this.getRoot());

		return stack;
	}

	/**
	 * voegt een kind toe aan de opgegeven treepositie
	 * 
	 * @param treepositie
	 * @param element
	 * @param side
	 *            ("links" of "rechts")
	 * @throws InvalidTreePositionException
	 * @throws InvalidSideException
	 */
	public void addChild(TreePosition<E> pos, E element, String side)
			throws InvalidTreePositionException, InvalidSideException {
		// behoort deze treeposition tot deze boom?
		boolean nieuwelement = false;

		TreeNode node = validate(pos);
		nieuwelement = node.addChild(element, side);
		if (nieuwelement)
			size++;
	}

	/**
	 * @param treepos
	 * @return de treepositie van het linkerkind van de gegeven treeposition
	 * @throws InvalidTreePositionException
	 */
	public TreePosition<E> getLeftChild(TreePosition<E> pos) throws InvalidTreePositionException {
		TreeNode Node = validate(pos);
		return (TreePosition<E>) Node.getLeftChild();
	}

	/**
	 * @param treepos
	 * @return de treepositie van het rechterkind van de gegeven treeposition
	 * @throws InvalidTreePositionException
	 */
	public TreePosition<E> getRightChild(TreePosition<E> pos) throws InvalidTreePositionException {
		TreeNode Node = validate(pos);
		return (TreePosition<E>) Node.getRightChild();
	}

	/**
	 * @param treepositie
	 * @return het level van de treepositie
	 * @throws InvalidTreePositionException
	 */
	public int getLevel(TreePosition<E> pos) throws InvalidTreePositionException {
		TreeNode node = validate(pos);
		return node.level(node);
	}

	/**
	 * valideert of de treeposition ook werkelijk een treenode is
	 * 
	 * @param de
	 *            treepositie
	 * @return de treenode
	 * @throws InvalidTreePositionException
	 */
	private TreeNode validate(TreePosition<E> pos) throws InvalidTreePositionException {
		if (pos != null && pos instanceof BinaryTree.TreeNode) {
			return (TreeNode) pos; // hier wel checken, want downcasten gebeurt niet automatisch
		} else {
			throw new InvalidTreePositionException();
		}
	}

	private class TreeNode implements TreePosition<E> {

		// attributen

		private TreeNode parent;
		private E element;
		private TreeNode leftChild;
		private TreeNode rightChild;

		// constructor
		public TreeNode(E element) {
			this(null, element);
		}

		// constructor
		public TreeNode(TreeNode parent, E element) {
			this.parent = parent;
			this.element = element;
			leftChild = null;
			rightChild = null;
		}

		/**
		 * @return het element in de node
		 */
		public E getElement() {
			return element;
		}

		/**
		 * voegt een kind toe aan een node
		 * 
		 * @param element
		 * @param kant
		 *            ("links" of "rechts"
		 * @return true als een nieuwe node moet aangemaakt worden
		 * @throws InvalidSideException
		 */
		public boolean addChild(E element, String kant) throws InvalidSideException {
			boolean nieuwelement = false;
			if (kant.equals("right")) {
				if (rightChild == null)
					nieuwelement = true;
				rightChild = new TreeNode(this, element);
			} else if (kant.equals("left")) {
				if (leftChild == null)
					nieuwelement = true;
				leftChild = new TreeNode(this, element);
			} else {
				throw new InvalidSideException();
			}
			return nieuwelement;
		}

		/**
		 * @param current
		 * @return het 'level' van de node
		 */
		public int level(TreeNode current) {
			if (current.isRoot()) {
				return 0;
			}
			return level(current.parent) + 1;
		}

		/**
		 * @return parent van de node
		 */
		public TreeNode getParent() {
			return parent;
		}

		/**
		 * @return linkerkind van de node
		 */
		public TreeNode getLeftChild() {
			return leftChild;
		}

		/**
		 * @return rechterkind van de node
		 */
		public TreeNode getRightChild() {
			return rightChild;
		}

		/**
		 * @return true als de node de root is
		 */

		public boolean isRoot() {
			return parent == null;
		}
	}
}
