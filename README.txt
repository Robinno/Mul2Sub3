Ik maakte gebruik van een binary tree, en zette de startwaarde in de root.

OPBOUW VAN DE TREE

Voor iedere node:
het linkerkind = waarde * X en het rechterkind = waarde - y

omdat het inefficiŽnt is om de hele tree op te bouwen naar beneden toe, moet de tree level per level (in de breedte) worden opgebouwd. 
Hiervoor heb ik een queue gebruikt:

binary tree  |	queue
-----------------------
      8	     |			dit is de root: nu worden de kinderen aangemaakt
-----------------------
      8      |
     / \     |     		Nu gaan we linkerkind en rechterkind toevoegen aan de queue
    16  5    |     		
-----------------------
      8      |
     / \     |  [16, 5]   	we springen nu naar het eerste element in de queue en herhalen	
    16  5    |     		
-----------------------
      8      |
     / \     |  [5]   		kinderen maken onder de 16, want dit was het eerste element in de queue
    16  5    |     
   /  \	     |
  32   13    |
-----------------------
      8      |
     / \     |  [5, 32, 13]   	de nieuwe kinderen toevoegen aan de queue 
    16  5    |     
   /  \	     |
  32   13    |

Dit nu herhalen (met een maximum diepte van 100): naar het eerste element springen (=5), die kinderen maken, toevoegen, ...

EENMAAL HET DOEL GEVONDEN

Starten die treenode met het te zoeken getal als element:
1. Toevoegen aan de stack
2. Verplaatsen naar de parent van die node
Dit herhalen totdat we aan de root zitten.

OPTIMALISEREN VAN HET ALGORITME

geoptimaliseerd door alle elementen uit de binary tree bij te houden in een linkedlist.
Voordat we het kind maken en toevoegen aan de queue kijken we of het element van het toekomstige kind al zit in de linkedlist. 
Indien ja: overslaan, geen kinderen maken, ook niet toevoegen aan queue. 
Indien nee: wel kind maken, toevoegen aan queue EN ook element toevoegen in de linkedlist!
